# -*- coding: utf-8 -*-
"""
Test vectors for JWE.

From:
https://hexdocs.pm/jose/JOSE.JWS.html

Linked source:
https://gist.github.com/potatosalad/925a8b74d85835e285b9
"""

# Created: 2018-08-06 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.

JWK_ED25519 = (
    '{"crv":"Ed25519"'
    ',"d":"VoU6Pm8SOjz8ummuRPsvoJQOPI3cjsdMfUhf2AAEc7s"'
    ',"kty":"OKP"'
    ',"x":"l11mBSuP-XxI0KoSG7YEWRp4GWm7dKMOPkItJy2tlMM"}')
JWS_SIGNED = (
    'eyJhbGciOiJFZDI1NTE5In0.'
    'e30.'
    'xyg2LTblm75KbLFJtROZRhEgAFJdlqH9bhx8a9LO1yvLxNLhO9fLqnFuU3ojOdbObr8bs'
    'ubPkPqUfZlPkGHXCQ'
)
JWS_CONTENT_DICT = {
    'header': {'alg': 'Ed25519'},
    'payload': {},
    'signature': 'xyg2LTblm75KbLFJtROZRhEgAFJdlqH9bhx8a9LO1yvLxNLhO9fLqnFu'
                 'U3ojOdbObr8bsubPkPqUfZlPkGHXCQ'
}
