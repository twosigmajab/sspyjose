# -*- coding: utf-8 -*-
"""Tests for the jws module."""

# Created: 2018-08-02 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.
#
# This work is licensed under the Apache 2.0 open source licence.
# Terms and conditions apply.
#
# You should have received a copy of the licence along with this
# program.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

import nacl
from unittest import mock  # @UnusedImport # noqa: F401
import unittest

from sspyjose import (Jose,
                      utils)
from sspyjose.jwk import (Jwk,
                          Ed25519Jwk)
from sspyjose.jws import (Jws,
                          Ed25519Jws)

from tests import jws_test_vectors as test_vectors
from tests.data import (JWK_ARTHUR_FULL_ED25519,
                        JWK_ARTHUR_PUB_ED25519,
                        JWK_FORD_FULL_ED25519,
                        JWS_ED25519_ARTHUR,
                        JWS_ED25519_ARTHUR_DICT,
                        JWS_ED25519_ARTHUR_COMPACT,
                        JWS_ED25519_FORD_DICT,
                        JWS_ED25519_ARTHUR_FORD,
                        JWS_ED25519_ARTHUR_FORD_DICT,
                        JWS_ED25519_FORD_COMPACT,
                        TEXT_CONTENT)


class JwsTest(unittest.TestCase):
    """Testing the Jws class."""

    arthur_jwk_priv = None
    ford_jwk_priv = None

    def setUp(self):  # noqa: D102
        self.arthur_jwk_priv = Ed25519Jwk(
            from_json=JWK_ARTHUR_FULL_ED25519)
        self.ford_jwk_priv = Ed25519Jwk(
            from_json=JWK_FORD_FULL_ED25519)

    def tearDown(self):  # noqa: D102
        pass

    def test_vanilla_constructor(self):
        """Make a vanilla/empty JWS."""
        jws = Jws()
        self.assertDictEqual(jws.header, {})
        self.assertEqual(jws.payload, None)
        self.assertEqual(jws.signature, None)
        self.assertEqual(jws.jwk, None)

    def test_constructor_from_compact(self):
        """Make JWS from compact serialisation."""
        jws = Jws(from_compact=JWS_ED25519_ARTHUR_COMPACT)
        self.assertDictEqual(jws.header, JWS_ED25519_ARTHUR_DICT['header'])
        self.assertDictEqual(jws.payload, JWS_ED25519_ARTHUR_DICT['payload'])
        self.assertEqual(jws._signature, JWS_ED25519_ARTHUR_DICT['signature'])
        self.assertEqual(jws.jwk, None)

    def test_constructor_from_json(self):
        """Make multi-sig JWS from JSON serialisation."""
        jws = Jws(from_json=JWS_ED25519_ARTHUR_FORD)
        self.assertDictEqual(jws.header, JWS_ED25519_ARTHUR_DICT['header'])
        self.assertDictEqual(jws.payload, JWS_ED25519_ARTHUR_DICT['payload'])
        self.assertListEqual(jws.signature,
                             [JWS_ED25519_ARTHUR_DICT['signature'],
                              JWS_ED25519_FORD_DICT['signature']])
        self.assertEqual(jws.jwk, None)

    def test_serialise_compact(self):
        """Conversion of signature to JWS compact format."""
        jws = Jws()
        jws.jwk = self.arthur_jwk_priv
        jws.header = JWS_ED25519_ARTHUR_DICT['header']
        jws.payload = JWS_ED25519_ARTHUR_DICT['payload']
        jws._signature = JWS_ED25519_ARTHUR_DICT['signature']
        self.assertEqual(jws.serialise(), JWS_ED25519_ARTHUR_COMPACT)

    def test_serialise_json(self):
        """Conversion of signature to JWS JSON format."""
        jws = Jws()
        jws.jwk = self.arthur_jwk_priv
        jws.header = JWS_ED25519_ARTHUR_DICT['header']
        jws.payload = JWS_ED25519_ARTHUR_DICT['payload']
        jws._signature = JWS_ED25519_ARTHUR_DICT['signature']
        result = jws.serialise(try_compact=False)
        self.assertEqual(result, JWS_ED25519_ARTHUR)
        self.assertEqual(result, jws.to_json())

    def test_serialise_multisig_json(self):
        """Conversion of multi-signature to JWS JSON format."""
        jws = Ed25519Jws()
        jws.jwk = self.arthur_jwk_priv
        jws.add_jwk(self.ford_jwk_priv)
        jws.header = JWS_ED25519_ARTHUR_DICT['header']
        jws.payload = JWS_ED25519_ARTHUR_DICT['payload']
        jws._signature = [JWS_ED25519_ARTHUR_DICT['signature'],
                          JWS_ED25519_FORD_DICT['signature']]
        self.arthur_jwk_priv._kid = None
        result = jws.serialise(try_compact=False)
        self.assertEqual(utils.json_to_dict(result),
                         JWS_ED25519_ARTHUR_FORD_DICT)

    def test_serialise_multisig_json_missing_kid(self):
        """Conversion of multi-sig to JWS JSON format with missing `kid`."""
        jws = Ed25519Jws(from_json=JWS_ED25519_ARTHUR_FORD)
        jws.jwk = self.arthur_jwk_priv
        jws.add_jwk(self.ford_jwk_priv)
        # Remove Ford's JWK's `kid`.
        jws._jwk[1]._data['kid'] = None
        jws.header = JWS_ED25519_ARTHUR_DICT['header']
        jws.payload = JWS_ED25519_ARTHUR_DICT['payload']
        jws._signature = [JWS_ED25519_ARTHUR_DICT['signature'],
                          JWS_ED25519_FORD_DICT['signature']]
        self.arthur_jwk_priv._kid = None
        result = jws.serialise(try_compact=False)
        to_check = utils.json_to_dict(result)
        expected_key_hash = 'yvt454GYBIeHUaaFnFLf7AepSSdYP0ZT24nnEzlU-94'
        self.assertEqual(to_check['signatures'][1]['header']['key_hash'],
                         expected_key_hash)

    def test_to_json(self):
        """Conversion of signature to JWS JSON format."""
        jws = Jws()
        jws.jwk = self.arthur_jwk_priv
        jws.header = JWS_ED25519_ARTHUR_DICT['header']
        jws.payload = JWS_ED25519_ARTHUR_DICT['payload']
        jws._signature = JWS_ED25519_ARTHUR_DICT['signature']
        self.assertEqual(jws.to_json(), JWS_ED25519_ARTHUR)

    def test_add_jwk_first(self):
        """Add a JWK for signing."""
        jws = Jws()
        jws.add_jwk(self.arthur_jwk_priv)
        self.assertIs(jws.jwk, self.arthur_jwk_priv)

    def test_add_jwk_second(self):
        """Add second JWK for signing."""
        jws = Jws()
        jws.jwk = self.arthur_jwk_priv
        jws.add_jwk(self.ford_jwk_priv)
        self.assertIsInstance(jws.jwk, list)
        self.assertEqual(len(jws.jwk), 2)
        self.assertIs(jws.jwk[0], self.arthur_jwk_priv)
        self.assertIs(jws.jwk[1], self.ford_jwk_priv)

    def test_add_jwk_third(self):
        """Add third JWK for signing."""
        jws = Jws()
        jws.jwk = self.arthur_jwk_priv
        jws.add_jwk(self.ford_jwk_priv)
        jws.add_jwk(self.arthur_jwk_priv)
        self.assertIsInstance(jws.jwk, list)
        self.assertEqual(len(jws.jwk), 3)
        self.assertIs(jws.jwk[2], self.arthur_jwk_priv)

    def test_get_instance_defaults(self):
        """Use defaults-only factory."""
        my_instance = Jws.get_instance()
        self.assertIsInstance(my_instance, Ed25519Jws)
        self.assertEqual(my_instance._DEFAULT_HEADER['alg'], Jose.DEFAULT_SIG)

    def test_get_instance_unsupported_cipher(self):
        """Use factory for an unsupported cipher."""
        self.assertRaises(RuntimeError, Jws.get_instance,
                          **dict(alg='ES256K'))


class Ed25519JwsTest(unittest.TestCase):
    """Testing the Ed25519Jws class."""

    arthur_jwk_priv = None
    arthur_jwk_pub = None
    ford_jwk_priv = None

    def setUp(self):  # noqa: D102
        self.arthur_jwk_priv = Ed25519Jwk(
            from_json=JWK_ARTHUR_FULL_ED25519)
        self.arthur_jwk_pub = Ed25519Jwk(
            from_json=JWK_ARTHUR_PUB_ED25519)
        self.ford_jwk_priv = Ed25519Jwk(
            from_json=JWK_FORD_FULL_ED25519)

    def tearDown(self):  # noqa: D102
        pass

    def test_sign(self):
        """Sign a header/payload."""
        tests = [(self.arthur_jwk_priv, JWS_ED25519_ARTHUR_DICT),
                 (self.ford_jwk_priv, JWS_ED25519_FORD_DICT)]
        for jwk, data in tests:
            jws = Ed25519Jws()
            jws.jwk = jwk
            jws.payload = data['payload']
            jws.sign()
            self.assertEqual(jws.signature, data['signature'])

    def test_sign_hhgttg(self):
        """Sign a HHGTTG header/payload."""
        tests = [(JWK_ARTHUR_FULL_ED25519, JWS_ED25519_ARTHUR_DICT,
                  JWS_ED25519_ARTHUR_COMPACT),
                 (JWK_FORD_FULL_ED25519, JWS_ED25519_FORD_DICT,
                  JWS_ED25519_FORD_COMPACT)]
        for key, data, result in tests:
            jwk = Ed25519Jwk(from_json=key)
            jws = Ed25519Jws(jwk=jwk)
            jws.payload = data['payload']
            jws.sign()
            self.assertEqual(jws.serialise(), result)

    def test_vector_sign(self):
        """JWS test vector signing."""
        jws = Ed25519Jws()
        jwk = Ed25519Jwk(from_json=test_vectors.JWK_ED25519)
        jws.jwk = jwk
        jws.payload = test_vectors.JWS_CONTENT_DICT['payload']
        jws.sign()
        self.assertDictEqual(jws.header,
                             test_vectors.JWS_CONTENT_DICT['header'])
        self.assertEqual(utils.bytes_to_string(jws.signature),
                         test_vectors.JWS_CONTENT_DICT['signature'])

    def test_vector_verify(self):
        """JWS test vector verification."""
        jws = Ed25519Jws(from_compact=test_vectors.JWS_SIGNED)
        jwk = Ed25519Jwk(from_json=test_vectors.JWK_ED25519)
        jws.jwk = jwk
        jws.verify()
        self.assertTrue(jws.verify())

    def test_verify(self):
        """Verify a signature."""
        tests = [(self.arthur_jwk_priv, JWS_ED25519_ARTHUR_DICT),
                 (self.ford_jwk_priv, JWS_ED25519_FORD_DICT)]
        for jwk, data in tests:
            jws = Ed25519Jws()
            jws.jwk = jwk
            jws.payload = data['payload']
            jws.signature = data['signature']
            self.assertTrue(jws.verify())

    def test_verify_hhgttg(self):
        """Verify HHGTTG signatures."""
        tests = [(JWK_ARTHUR_FULL_ED25519, JWS_ED25519_ARTHUR_COMPACT),
                 (JWK_FORD_FULL_ED25519, JWS_ED25519_FORD_COMPACT)]
        for key, data in tests:
            jwk = Ed25519Jwk(from_json=key)
            jws = Ed25519Jws(jwk=jwk)
            jws.load_compact(data)
            self.assertTrue(jws.verify())

    def test_verify_fail(self):
        """Verify a bad signature."""
        jws = Ed25519Jws()
        jws.jwk = self.arthur_jwk_pub
        jws.header = JWS_ED25519_ARTHUR_DICT['header']
        jws.payload = JWS_ED25519_ARTHUR_DICT['payload'].copy()
        jws.payload['advice'] = 'Bad advice'
        jws.signature = JWS_ED25519_ARTHUR_DICT['signature']
        self.assertRaises(nacl.exceptions.BadSignatureError, jws.verify)

    def test_multi_sign(self):
        """Multiple signatures to a header/payload."""
        jws = Ed25519Jws()
        jws.jwk = self.arthur_jwk_priv
        jws.add_jwk(self.ford_jwk_priv)
        jws.payload = JWS_ED25519_ARTHUR_DICT['payload']
        jws.sign()
        self.assertListEqual(jws.signature,
                             [JWS_ED25519_ARTHUR_DICT['signature'],
                              JWS_ED25519_FORD_DICT['signature']])

    def test_multi_verify(self):
        """Verify multiple signatures on a header/payload."""
        jws = Ed25519Jws()
        jws.jwk = self.arthur_jwk_priv
        jws.add_jwk(self.ford_jwk_priv)
        jws.payload = JWS_ED25519_ARTHUR_DICT['payload']
        jws.signature = [JWS_ED25519_ARTHUR_DICT['signature'],
                         JWS_ED25519_FORD_DICT['signature']]
        self.assertTrue(jws.verify())

    def test_multi_verify_missing_key(self):
        """Fail a multi-sig verify on a header/payload with missing keys."""
        jws = Ed25519Jws()
        jws.jwk = self.arthur_jwk_priv
        jws.payload = JWS_ED25519_ARTHUR_DICT['payload']
        jws.signature = [JWS_ED25519_ARTHUR_DICT['signature'],
                         JWS_ED25519_FORD_DICT['signature']]
        self.assertRaises(RuntimeError, jws.verify)

    def test_round_trips(self):
        """JWS sign/verify round trips."""
        for text in TEXT_CONTENT:
            message = {'text': text}
            signer = Ed25519Jws(jwk=self.arthur_jwk_priv)
            signer.payload = message
            signer.sign()
            verifier = Ed25519Jws(
                from_compact=signer.serialise(try_compact=True),
                jwk=self.arthur_jwk_pub)
            self.assertTrue(verifier.verify())


class FactoryUsageTest(unittest.TestCase):
    """Testing the JWS via the factories exclusively."""

    def setUp(self):  # noqa: D102
        pass

    def tearDown(self):  # noqa: D102
        pass

    def test_round_trips_defaults(self):
        """JWS sign/verify round trips using factory defaults."""
        priv_jwk = Jwk.get_instance(
            crv='Ed25519', from_json=test_vectors.JWK_ED25519)
        for text in TEXT_CONTENT:
            message = {'text': text}
            signer = Jws.get_instance(jwk=priv_jwk)
            signer.payload = message
            signer.sign()
            verifier = Jws.get_instance(
                from_compact=signer.serialise(try_compact=True),
                jwk=priv_jwk)
            self.assertTrue(verifier.verify())


if __name__ == '__main__':
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
