# -*- coding: utf-8 -*-
"""Tests data for SingleSource PyJOSE."""

# Created: 2018-07-26 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.
#
# This work is licensed under the Apache 2.0 open source licence.
# Terms and conditions apply.
#
# You should have received a copy of the licence along with this
# program.


__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

JWK_ARTHUR_FULL_ED25519 = (
    '{"kty":"OKP"'
    ',"crv":"Ed25519"'
    ',"d":"nWGxne_9WmC6hEr0kuwsxERJxWl7MmkZcDusAxyuf2A"'
    ',"x":"11qYAYKxCrfVS_7TyWQHOg7hcvPapiMlrwIaaPcHURo"'
    ',"kid":"Arthur Dent, Earth"}')
JWK_ARTHUR_PUB_ED25519 = (
    '{"kty":"OKP"'
    ',"crv":"Ed25519"'
    ',"x":"11qYAYKxCrfVS_7TyWQHOg7hcvPapiMlrwIaaPcHURo"'
    ',"kid":"Arthur Dent, Earth"}')
JWK_ARTHUR_FULL_ED25519_DICT = {
    'kty': 'OKP',
    'crv': 'Ed25519',
    'd': b'\x9da\xb1\x9d\xef\xfdZ`\xba\x84J\xf4\x92\xec,\xc4DI\xc5i{2i'
         b'\x19p;\xac\x03\x1c\xae\x7f`',
    'x': b'\xd7Z\x98\x01\x82\xb1\n\xb7\xd5K\xfe\xd3\xc9d\x07:\x0e\xe1r'
         b'\xf3\xda\xa6#%\xaf\x02\x1ah\xf7\x07Q\x1a',
    'kid': 'Arthur Dent, Earth'
}
JWK_FORD_PUB_ED25519 = (
    '{"kty":"OKP"'
    ',"crv":"Ed25519"'
    ',"x":"8TakswfV80LFiX_Ii1dJL9u4vqWlkhApKbo-v2i1uUc"'
    ',"kid":"Ford Prefect, Betelgeuse"}')
JWK_FORD_FULL_ED25519 = (
    '{"kty":"OKP"'
    ',"crv":"Ed25519"'
    ',"d":"Y-pua--r4vGiRcZieji_sdH5feNsRJgTXBEiOlEmfWc"'
    ',"x":"8TakswfV80LFiX_Ii1dJL9u4vqWlkhApKbo-v2i1uUc"'
    ',"kid":"Ford Prefect, Betelgeuse"}')
JWK_FORD_FULL_ED25519_DICT = {
    'kty': 'OKP',
    'crv': 'Ed25519',
    'd': b'c\xeank\xef\xab\xe2\xf1\xa2E\xc6bz8\xbf\xb1\xd1\xf9}\xe3lD\x98'
         b'\x13\\\x11":Q&}g',
    'x': b'\xf16\xa4\xb3\x07\xd5\xf3B\xc5\x89\x7f\xc8\x8bWI/\xdb\xb8\xbe'
         b'\xa5\xa5\x92\x10))\xba>\xbfh\xb5\xb9G',
    'kid': 'Ford Prefect, Betelgeuse'
}
JWK_ARTHUR_FULL_X25519 = (
    '{"kty":"OKP"'
    ',"crv":"X25519"'
    ',"d":"U7ZBkzSFDI7uUV0RfDmFaeHxUCnFaxaib9sypeLrJ-M"'
    ',"x":"U1GCoOv8N0lePEpZVL0eWEOxriqW_ikAnZdDq4nAn24"'
    ',"kid":"Arthur Dent, Earth"}')
JWK_ARTHUR_PUB_X25519 = (
    '{"kty":"OKP"'
    ',"crv":"X25519"'
    ',"x":"U1GCoOv8N0lePEpZVL0eWEOxriqW_ikAnZdDq4nAn24"'
    ',"kid":"Arthur Dent, Earth"}')
JWK_ARTHUR_FULL_X25519_DICT = {
    'kty': 'OKP',
    'crv': 'X25519',
    'd': b"S\xb6A\x934\x85\x0c\x8e\xeeQ]\x11|9\x85i\xe1\xf1P)\xc5k\x16"
         b"\xa2o\xdb2\xa5\xe2\xeb'\xe3",
    'x': b'SQ\x82\xa0\xeb\xfc7I^<JYT\xbd\x1eXC\xb1\xae*\x96\xfe)\x00\x9d'
         b'\x97C\xab\x89\xc0\x9fn',
    'kid': 'Arthur Dent, Earth'
}
JWK_FORD_FULL_X25519 = (
    '{"kty":"OKP"'
    ',"crv":"X25519"'
    ',"d":"vfoxXOfEJsZXwzgOVLEK7kmqgcaX0gXtYaEFD2tlwbA"'
    ',"x":"kVNmr6NqCs1mjtprt4sK01Is6J39jpprRz4Ky3Ig-1M"'
    ',"kid":"Ford Prefect, Betelgeuse"}')
JWK_FORD_PUB_X25519 = (
    '{"kty":"OKP"'
    ',"crv":"X25519"'
    ',"x":"kVNmr6NqCs1mjtprt4sK01Is6J39jpprRz4Ky3Ig-1M"'
    ',"kid":"Ford Prefect, Betelgeuse"}')
JWK_CHACHA20POLY1305 = (
    '{"kty":"oct"'
    ',"alg":"C20P"'
    ',"k":"EffEuY2nbShIVtizmek8AuR7ftSuY2e8XRxGjMc8QAc"'
    ',"use":"enc"'
    ',"kid":"Sub-Ether key"}')
JWK_CHACHA20POLY1305_DICT = {
    'kty': 'oct',
    'alg': 'C20P',
    'k': b'\x11\xf7\xc4\xb9\x8d\xa7m(HV\xd8\xb3\x99\xe9<\x02\xe4{~\xd4\xae'
         b'cg\xbc]\x1cF\x8c\xc7<@\x07',
    'use': 'enc',
    'kid': 'Sub-Ether key'
}
JWK_AES256GCM = (
    '{"kty":"oct"'
    ',"alg":"A256GCM"'
    ',"k":"EffEuY2nbShIVtizmek8AuR7ftSuY2e8XRxGjMc8QAc"'
    ',"use":"enc"'
    ',"kid":"Sub-Ether key"}')
JWK_AES256GCM_DICT = {
    'kty': 'oct',
    'alg': 'A256GCM',
    'k': b'\x11\xf7\xc4\xb9\x8d\xa7m(HV\xd8\xb3\x99\xe9<\x02\xe4{~\xd4\xae'
         b'cg\xbc]\x1cF\x8c\xc7<@\x07',
    'use': 'enc',
    'kid': 'Sub-Ether key'
}

JWE_CHACHA20POLY1305 = (
    'eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0.'
    '.'
    'lbsERynEgQS8CRXZ.'
    'Gb4_5z9ib8DBowDctZQc1y0Ao_xCKzzaqQ.'
    'ZN-LKOvAidtnrqB6dz-EVQ')
JWE_CHACHA20POLY1305_DECODED = {
    'header': {'alg': 'dir', 'enc': 'C20P'},
    'key': None,
    'nonce': b'\x95\xbb\x04G)\xc4\x81\x04\xbc\t\x15\xd9',
    'ciphertext': b'\x19\xbe?\xe7?bo\xc0\xc1\xa3\x00\xdc\xb5\x94\x1c\xd7-'
                  b'\x00\xa3\xfcB+<\xda\xa9',
    'tag': b'd\xdf\x8b(\xeb\xc0\x89\xdbg\xae\xa0zw?\x84U'
}
JWE_CHACHA20POLY1305_MESSAGE = {'advice': "Don't panic!"}

JWE_AES256GCM = (
    'eyJhbGciOiJkaXIiLCJlbmMiOiJBMjU2R0NNIn0.'
    '.'
    'lbsERynEgQS8CRXZ.'
    'lM32nqK0BEj3rzgS8JFgVeZn0jkGJJ9bsg.'
    'aM2Re1540HYcoAPfAzbLGA')
JWE_AES256GCM_DECODED = {
    'header': {'alg': 'dir', 'enc': 'C20P'},
    'key': None,
    'nonce': b'\x95\xbb\x04G)\xc4\x81\x04\xbc\t\x15\xd9',
    'ciphertext': b'\x94\xcd\xf6\x9e\xa2\xb4\x04H\xf7\xaf8\x12\xf0\x91`U'
                  b'\xe6g\xd29\x06$\x9f[\xb2',
    'tag': b'h\xcd\x91{^x\xd0v\x1c\xa0\x03\xdf\x036\xcb\x18'
}
JWE_AES256GCM_MESSAGE = {'advice': "Don't panic!"}

JWE_UNSECURED = (
    'eyJhbGciOiJ1bnNlY3VyZWQifQ.'
    '.'
    '.'
    'eyJhZHZpY2UiOiJEb24ndCBwYW5pYyEifQ.'
    '')
JWE_UNSECURED_DECODED = {
    'header': {'alg': 'unsecured'},
    'key': None,
    'nonce': None,
    'ciphertext': b'{"advice":"Don\'t panic!"}',
    'tag': None
}
JWE_UNSECURED_MESSAGE = {'advice': "Don't panic!"}

JWE_X25519CHACHA20POLY1305 = (
    'eyJhbGciOiJFQ0RILUVTIiwiZW5jIjoiQzIwUCIsImVwayI6eyJrdHkiOiJPS1AiLCJjcn'
    'YiOiJYMjU1MTkiLCJ4IjoiVTFHQ29PdjhOMGxlUEVwWlZMMGVXRU94cmlxV19pa0FuWmRE'
    'cTRuQW4yNCIsImtpZCI6IkFydGh1ciBEZW50LCBFYXJ0aCJ9fQ.'
    '.'
    'lbsERynEgQS8CRXZ.'
    'msRbmuOiXKAYTQEiacfuHOffSSIknj0h-A.'
    '-DVVZdiB7X-yKNrZc_AIBw')
JWE_X25519CHACHA20POLY1305_DECODED = {
    'header': {
        'alg': 'ECDH-ES', 'enc': 'C20P',
        'epk': {  # Ford's public X25519 JWK (mocked in).
            "kty": "OKP",
            "crv": "X25519",
            "x": "U1GCoOv8N0lePEpZVL0eWEOxriqW_ikAnZdDq4nAn24"
        }},
    'key': None,
    'nonce': b'\x95\xbb\x04G)\xc4\x81\x04\xbc\t\x15\xd9',
    'ciphertext': b'\x9a\xc4[\x9a\xe3\xa2\\\xa0\x18M\x01"i\xc7\xee\x1c\xe7'
                  b'\xdfI"$\x9e=!\xf8',
    'tag': b'\xf85Ue\xd8\x81\xed\x7f\xb2(\xda\xd9s\xf0\x08\x07'
}
JWE_X25519CHACHA20POLY1305_MESSAGE = {'advice': "Don't panic!"}

JWS_ED25519_ARTHUR = (
    '{"header":"eyJhbGciOiJFZDI1NTE5In0"'
    ',"payload":"eyJhZHZpY2UiOiJEb24ndCBwYW5pYyEifQ"'
    ',"signature":"Df9K5X1o-oHtS_X3RDZIlDxIH6BMRRkg_eQoA5XZUe-qmhIaPqoabw'
    'EHBBR2MMqEbm1SpT7a1s9VwAxZJrE0Bg"}')
JWS_ED25519_ARTHUR_COMPACT = (
    'eyJhbGciOiJFZDI1NTE5In0.'
    'eyJhZHZpY2UiOiJEb24ndCBwYW5pYyEifQ.'
    'Df9K5X1o-oHtS_X3RDZIlDxIH6BMRRkg_eQoA5XZUe-qmhIaPqoabwEHBBR2MMqEbm1S'
    'pT7a1s9VwAxZJrE0Bg')
JWS_ED25519_ARTHUR_DICT = {
    'header': {'alg': 'Ed25519'},
    'payload': {'advice': "Don't panic!"},
    'signature': b'\r\xffJ\xe5}h\xfa\x81\xedK\xf5\xf7D6H\x94<H\x1f\xa0LE'
                 b'\x19 \xfd\xe4(\x03\x95\xd9Q\xef\xaa\x9a\x12\x1a>\xaa'
                 b'\x1ao\x01\x07\x04\x14v0\xca\x84nmR\xa5>\xda\xd6\xcfU'
                 b'\xc0\x0cY&\xb14\x06',
}
JWS_ED25519_FORD = {
    '{"header":"eyJhbGciOiJFZDI1NTE5In0"'
    ',"payload":"eyJhZHZpY2UiOiJTdGF5IGZyb29keSEifQ"'
    ',"signature":"d-QLYgrusOMupVC506yoDEjSd2yQ62MBi5PtmvdTebQju_14T6xW93'
    'NuRnGcrA1OvqBex9Ql-pDwLaTCS9j5BQ"}'
}
JWS_ED25519_FORD_COMPACT = (
    'eyJhbGciOiJFZDI1NTE5In0.'
    'eyJhZHZpY2UiOiJEb24ndCBwYW5pYyEifQ.'
    'd-QLYgrusOMupVC506yoDEjSd2yQ62MBi5PtmvdTebQju_14T6xW93NuRnGcrA1OvqBe'
    'x9Ql-pDwLaTCS9j5BQ')
JWS_ED25519_FORD_DICT = {
    'header': {'alg': 'Ed25519'},
    'payload': {'advice': "Don't panic!"},
    'signature': b'w\xe4\x0bb\n\xee\xb0\xe3.\xa5P\xb9\xd3\xac\xa8\x0cH\xd2'
                 b'wl\x90\xebc\x01\x8b\x93\xed\x9a\xf7Sy\xb4#\xbb\xfdxO'
                 b'\xacV\xf7snFq\x9c\xac\rN\xbe\xa0^\xc7\xd4%\xfa\x90\xf0-'
                 b'\xa4\xc2K\xd8\xf9\x05'
}
# Analogous to RFC 7515, Aappendix A.6.4
# https://tools.ietf.org/html/rfc7515#appendix-A.6.4
JWS_ED25519_ARTHUR_FORD = (
    '{"payload":"eyJhZHZpY2UiOiJEb24ndCBwYW5pYyEifQ"'
    ',"signatures":['
    '{"header":{"kid":"Arthur Dent, Earth"}'
    ',"protected":"eyJhbGciOiJFZDI1NTE5In0"'
    ',"signature":"Df9K5X1o-oHtS_X3RDZIlDxIH6BMRRkg_eQoA5XZUe-qmhIa'
    'PqoabwEHBBR2MMqEbm1SpT7a1s9VwAxZJrE0Bg"}'
    ',{"header":{"kid":"Ford Prefect, Betelgeuse"}'
    ',"protected":"eyJhbGciOiJFZDI1NTE5In0"'
    ',"signature":"d-QLYgrusOMupVC506yoDEjSd2yQ62MBi5PtmvdTebQju_14'
    'T6xW93NuRnGcrA1OvqBex9Ql-pDwLaTCS9j5BQ"'
    '}]}')
JWS_ED25519_ARTHUR_FORD_DICT = {
    'payload': 'eyJhZHZpY2UiOiJEb24ndCBwYW5pYyEifQ',
    'signatures': [
        {
            'header': {'kid': 'Arthur Dent, Earth'},
            'protected': 'eyJhbGciOiJFZDI1NTE5In0',
            'signature': 'Df9K5X1o-oHtS_X3RDZIlDxIH6BMRRkg_eQoA5XZUe-qmhIa'
                         'PqoabwEHBBR2MMqEbm1SpT7a1s9VwAxZJrE0Bg'
        },
        {
            'header': {'kid': 'Ford Prefect, Betelgeuse'},
            'protected': 'eyJhbGciOiJFZDI1NTE5In0',
            'signature': 'd-QLYgrusOMupVC506yoDEjSd2yQ62MBi5PtmvdTebQju_14'
                         'T6xW93NuRnGcrA1OvqBex9Ql-pDwLaTCS9j5BQ'
        }
    ]
}
TEXT_CONTENT = ['42', "Don't panic!", 'Flying Spaghetti Monster',
                "Ph'nglui mglw'nafh Cthulhu R'lyeh wgah'nagl fhtagn",
                'Tēnā koe', 'Hänsel & Gretel', 'Слартибартфаст']
