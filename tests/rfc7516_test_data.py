# -*- coding: utf-8 -*-
"""
AEAD test vectors for AES256-GCM from RFC 7516.
"""

# Created: 2019-03-08 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

TEST_VECTOR = {
    # Content Encryption Key (CEK) from Section A1.2
    'key': bytes(
        [177, 161, 244, 128, 84, 143, 225, 115, 63, 180, 3, 255, 107,
         154, 212, 246, 138, 7, 110, 91, 112, 46, 34, 105, 47, 130, 203,
         46, 122, 234, 64, 252]),
    # Initialization Vector from Section A1.4
    'nonce': bytes(
        [227, 197, 117, 252, 2, 219, 233, 68, 180, 225, 77, 219]),
    # Initialization Vector from Section A1
    'message': b'The true sign of intelligence is not knowledge but'
               b' imagination.',
    # Additional Authenticated Data from Section A1.5
    # BASE64URL(UTF8('{"alg":"RSA-OAEP","enc":"A256GCM"}'))
    'aad': b'eyJhbGciOiJSU0EtT0FFUCIsImVuYyI6IkEyNTZHQ00ifQ',
    # Encrypted content and tag from Section A1.6
    'ciphertext': bytes(
        [229, 236, 166, 241, 53, 191, 115, 196, 174, 43, 73, 109, 39,
         122, 233, 96, 140, 206, 120, 52, 51, 237, 48, 11, 190, 219, 186,
         80, 111, 104, 50, 142, 47, 167, 59, 61, 181, 127, 196, 21, 40,
         82, 242, 32, 123, 143, 168, 226, 73, 216, 176, 144, 138, 247,
         106, 60, 16, 205, 160, 109, 64, 63, 192]),
    'tag': bytes(
        [92, 80, 104, 49, 133, 25, 161, 215, 173, 101, 219, 211, 136, 91,
         210, 145])
}
