# -*- coding: utf-8 -*-
"""
Tests for the jwe module.
"""

# Created: 2018-07-25 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.
#
# This work is licensed under the Apache 2.0 open source licence.
# Terms and conditions apply.
#
# You should have received a copy of the licence along with this
# program.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

import base64
import cryptography
import json
import nacl
from unittest import mock  # @UnusedImport
import unittest

from sspyjose import Jose
from sspyjose.jwe import (Jwe,
                          ChaCha20Poly1305Jwe,
                          AES256GCMJwe,
                          UnsecuredJwe,
                          X25519ChaCha20Poly1305Jwe,
                          X25519AES256GCMJwe)
from sspyjose.jwk import (Jwk,
                          ChaCha20Poly1305Jwk,
                          AES256GCMJwk,
                          X25519Jwk)

from tests import jwe_test_vectors as test_vectors
from tests.data import (JWE_CHACHA20POLY1305,
                        JWE_CHACHA20POLY1305_DECODED,
                        JWE_CHACHA20POLY1305_MESSAGE,
                        JWE_AES256GCM,
                        JWE_AES256GCM_DECODED,
                        JWE_AES256GCM_MESSAGE,
                        JWE_UNSECURED,
                        JWE_UNSECURED_DECODED,
                        JWE_UNSECURED_MESSAGE,
                        JWE_X25519CHACHA20POLY1305,
                        JWE_X25519CHACHA20POLY1305_DECODED,
                        JWE_X25519CHACHA20POLY1305_MESSAGE,
                        JWK_CHACHA20POLY1305,
                        JWK_AES256GCM,
                        TEXT_CONTENT,
                        JWK_ARTHUR_FULL_X25519,
                        JWK_FORD_PUB_X25519,
                        JWK_FORD_FULL_X25519)


class JweTest(unittest.TestCase):
    """Testing the Jwe class."""

    symmetric_jwk = None

    def setUp(self):  # noqa: D102
        self.symmetric_jwk = ChaCha20Poly1305Jwk(
            from_json=JWK_CHACHA20POLY1305)

    def tearDown(self):  # noqa: D102
        pass

    def test_vanilla_constructor(self):
        """Make a vanilla/empty JWE."""
        jwe = Jwe()
        self.assertEqual(jwe.header, None)
        self.assertEqual(jwe.key, None)
        self.assertEqual(jwe.nonce, None)
        self.assertEqual(jwe.ciphertext, None)
        self.assertEqual(jwe.tag, None)
        self.assertEqual(jwe.message, None)
        self.assertEqual(jwe.jwk, None)
        self.assertEqual(jwe._message_bytes, None)

    def test_constructor_with_jwe_binary(self):
        """Make a JWE from JWE binary data."""
        jwe = Jwe(from_compact=JWE_CHACHA20POLY1305)
        self.assertEqual(jwe.header,
                         JWE_CHACHA20POLY1305_DECODED['header'])
        self.assertEqual(jwe.key, JWE_CHACHA20POLY1305_DECODED['key'])
        self.assertEqual(jwe.nonce, JWE_CHACHA20POLY1305_DECODED['nonce'])
        self.assertEqual(jwe.ciphertext,
                         JWE_CHACHA20POLY1305_DECODED['ciphertext'])
        self.assertEqual(jwe.tag, JWE_CHACHA20POLY1305_DECODED['tag'])
        self.assertEqual(jwe.message, None)
        self.assertEqual(jwe.jwk, None)
        self.assertEqual(jwe._message_bytes, None)

    def test_constructor_with_jwk(self):
        """Make a JWE with a JWK."""
        jwe = Jwe(jwk=self.symmetric_jwk)
        self.assertEqual(jwe.header, None)
        self.assertEqual(jwe.key, None)
        self.assertEqual(jwe.nonce, None)
        self.assertEqual(jwe.ciphertext, None)
        self.assertEqual(jwe.tag, None)
        self.assertEqual(jwe.message, None)
        self.assertEqual(jwe._message_bytes, None)
        self.assertEqual(jwe.jwk, self.symmetric_jwk)

    def test_jwk_attribute(self):
        """Setting/getting the jwk attribute."""
        jwe = Jwe()
        self.assertEqual(jwe.jwk, None)
        jwe.jwk = self.symmetric_jwk
        self.assertEqual(jwe.jwk, self.symmetric_jwk)

    def test_message_attribute(self):
        """Setting/getting the message attribute."""
        jwe = Jwe()
        my_message = {'answer': "Don't panic!"}
        jwe.ciphertext = b'foo'
        jwe._message_bytes = b'deadbeef'
        self.assertEqual(jwe.message, None)
        self.assertNotEqual(jwe.ciphertext, None)
        jwe.message = my_message
        self.assertEqual(jwe.message, my_message)
        self.assertEqual(jwe._message_bytes, None)
        self.assertEqual(jwe.ciphertext, None)
        self.assertEqual(jwe.tag, None)

    def test_ciphertext_attribute(self):
        """Setting/getting the ciphertext attribute."""
        jwe = Jwe()
        my_ciphertext = b'foo'
        jwe.message = b"Don't panic!"
        self.assertEqual(jwe.ciphertext, None)
        self.assertNotEqual(jwe.message, None)
        jwe.ciphertext = my_ciphertext
        self.assertEqual(jwe.ciphertext, my_ciphertext)
        self.assertEqual(jwe.message, None)

    def test_tag_attribute(self):
        """Setting/getting the tag attribute."""
        jwe = Jwe()
        my_tag = b'foo'
        self.assertEqual(jwe.tag, None)
        jwe.tag = my_tag
        self.assertEqual(jwe.tag, my_tag)

    def test_load_compact(self):
        """Load an empty JWE with JWE binary data."""
        jwe = Jwe()
        jwe.load_compact(JWE_CHACHA20POLY1305)
        self.assertDictEqual(jwe.header,
                             JWE_CHACHA20POLY1305_DECODED['header'])
        self.assertEqual(jwe.key, JWE_CHACHA20POLY1305_DECODED['key'])
        self.assertEqual(jwe.nonce, JWE_CHACHA20POLY1305_DECODED['nonce'])
        self.assertEqual(jwe.ciphertext,
                         JWE_CHACHA20POLY1305_DECODED['ciphertext'])
        self.assertEqual(jwe.tag, JWE_CHACHA20POLY1305_DECODED['tag'])
        self.assertEqual(jwe.message, None)
        self.assertEqual(jwe._message_bytes, None)
        self.assertEqual(jwe.jwk, None)

    def test_load_compact_wrong_no_of_parts(self):
        """Load incompatible JWE binary parts data."""
        jwe = Jwe()
        tests = [b'hydrogen.helium.lithium',
                 'hydrogen.helium.lithium.beryllium.boron.carbon']
        for test in tests:
            self.assertRaises(ValueError, jwe.load_compact, test)

    def test_to_json(self):
        """Serialises JWE to JSON."""
        jwe = Jwe(from_compact=JWE_CHACHA20POLY1305)
        result = jwe.to_json()
        compact = '.'.join([item if item else ''
                            for item in json.loads(result).values()])
        self.assertEqual(compact, JWE_CHACHA20POLY1305)

    def test_json_attribute(self):
        """Serialises JWE to JSON via the json attributge."""
        jwe = Jwe(from_compact=JWE_CHACHA20POLY1305)
        compact = '.'.join([item if item else ''
                            for item in json.loads(jwe.json).values()])
        self.assertEqual(compact, JWE_CHACHA20POLY1305)

    def test_serialise(self):
        """Serialises to JWE compact serialisation."""
        jwe = Jwe(from_compact=JWE_CHACHA20POLY1305)
        self.assertEqual(jwe.serialise(), JWE_CHACHA20POLY1305)

    def test_get_message_bytes(self):
        """Getting message binary representation."""
        jwe = Jwe()
        jwe.message = {'answer': "Don't panic!"}
        check = b'{"answer":"Don\'t panic!"}'
        self.assertEqual(jwe._message_bytes, None)
        self.assertEqual(jwe.get_message_bytes(), check)
        self.assertEqual(jwe._message_bytes, check)

    def test_get_instance_defaults(self):
        """Use defaults-only factory."""
        crypter = Jwe.get_instance()
        self.assertIsInstance(crypter, ChaCha20Poly1305Jwe)
        self.assertEqual(crypter._DEFAULT_HEADER['enc'], Jose.DEFAULT_ENC)

    def test_get_instance_specific_keys(self):
        """Use factory for specific keys."""
        tests = [('C20P', None, ChaCha20Poly1305Jwe),
                 ('A256GCM', None, AES256GCMJwe),
                 (None, 'ECDH-ES', X25519ChaCha20Poly1305Jwe),
                 ('A256GCM', 'ECDH-ES', X25519AES256GCMJwe)]
        for enc, alg, klass in tests:
            crypter = Jwe.get_instance(**{'enc': enc, 'alg': alg})
            self.assertIsInstance(crypter, klass)
            enc_check = enc or Jose.DEFAULT_ENC
            alg_check = alg or 'dir'
            self.assertEqual(crypter._DEFAULT_HEADER['enc'], enc_check)
            self.assertEqual(crypter._DEFAULT_HEADER['alg'], alg_check)

    def test_get_instance_unsupported_cipher(self):
        """Use factory for an unsupported cipher."""
        self.assertRaises(RuntimeError, Jwk.get_instance,
                          **dict(alg='AES128GCM'))

    def test_get_instance_with_jwk(self):
        """Use factory init with a given JWK."""
        tests = [(JWK_CHACHA20POLY1305, ChaCha20Poly1305Jwe),
                 (JWK_AES256GCM, AES256GCMJwe)]
        for key_json, klass in tests:
            key = Jwk.get_instance(from_json=key_json)
            crypter = Jwe.get_instance(jwk=key)
            self.assertIsInstance(crypter, klass)

    def test_get_instance_from_compact(self):
        """Use factory init with a compact JWE string."""
        tests = [(JWE_CHACHA20POLY1305, ChaCha20Poly1305Jwe),
                 (JWE_AES256GCM, AES256GCMJwe),
                 (JWE_X25519CHACHA20POLY1305, X25519ChaCha20Poly1305Jwe)]
        for data, klass in tests:
            crypter = Jwe.get_instance(from_compact=data)
            self.assertIsInstance(crypter, klass)
            self.assert_(len(crypter._nonce) > 0)
            self.assert_(len(crypter._ciphertext) > 0)
            self.assert_(len(crypter._tag) > 0)
            self.assertIsInstance(crypter._header, dict)


class ChaCha20Poly1305JweTest(unittest.TestCase):
    """Testing the ChaCha20Poly1305Jwe class."""

    symmetric_jwk = None
    my_jwe = None

    def setUp(self):  # noqa: D102
        self.symmetric_jwk = ChaCha20Poly1305Jwk(
            from_json=JWK_CHACHA20POLY1305)
        self.my_jwe = ChaCha20Poly1305Jwe()

    def tearDown(self):  # noqa: D102
        pass

    def test_vanilla_constructor(self):
        """Make a vanilla/empty JWE."""
        jwe = ChaCha20Poly1305Jwe()
        self.assertEqual(jwe.header, None)
        self.assertEqual(jwe.key, None)
        self.assertEqual(jwe.nonce, None)
        self.assertEqual(jwe.ciphertext, None)
        self.assertEqual(jwe.tag, None)
        self.assertEqual(jwe.message, None)
        self.assertEqual(jwe.jwk, None)
        self.assertEqual(jwe._message_bytes, None)

    def test_constructor_via_factory(self):
        """Make an empty JWE via the factory."""
        jwe = Jwe.get_instance(enc='C20P')
        self.assertIsInstance(jwe, ChaCha20Poly1305Jwe)
        self.assertEqual(jwe._DEFAULT_HEADER['enc'], 'C20P')
        self.assertEqual(jwe.header, None)
        self.assertEqual(jwe.key, None)
        self.assertEqual(jwe.nonce, None)
        self.assertEqual(jwe.ciphertext, None)
        self.assertEqual(jwe.tag, None)
        self.assertEqual(jwe.message, None)
        self.assertEqual(jwe.jwk, None)
        self.assertEqual(jwe._message_bytes, None)

    @mock.patch('nacl.utils.random', autospec=True,
                return_value=JWE_CHACHA20POLY1305_DECODED['nonce'])
    def test_encrypt(self, random_mock):
        """Encrypt a JWE message."""
        self.my_jwe.jwk = self.symmetric_jwk
        self.my_jwe.message = JWE_CHACHA20POLY1305_MESSAGE
        self.assertIs(self.my_jwe.nonce, None)
        result = self.my_jwe.encrypt()
        self.assertEqual(random_mock.call_count, 1)
        ciphertext = JWE_CHACHA20POLY1305_DECODED['ciphertext']
        tag = JWE_CHACHA20POLY1305_DECODED['tag']
        self.assertIsNot(self.my_jwe.nonce, None)
        self.assertEqual(result[0], ciphertext)
        self.assertEqual(result[1], tag)
        self.assertEqual(self.my_jwe.ciphertext, ciphertext)
        self.assertEqual(self.my_jwe._message_bytes,
                         b'{"advice":"Don\'t panic!"}')

    def test_decrypt(self):
        """Decrypt a JWE message."""
        self.my_jwe.load_compact(JWE_CHACHA20POLY1305)
        self.my_jwe.jwk = self.symmetric_jwk
        result = self.my_jwe.decrypt()
        self.assertDictEqual(result, JWE_CHACHA20POLY1305_MESSAGE)
        self.assertEqual(self.my_jwe.message, JWE_CHACHA20POLY1305_MESSAGE)
        self.assertEqual(self.my_jwe._message_bytes,
                         b'{"advice":"Don\'t panic!"}')

    def test_round_trips(self):
        """JWE en-/decrypt round trips."""
        for text in TEXT_CONTENT:
            message = {'text': text}
            encrypter = ChaCha20Poly1305Jwe(jwk=self.symmetric_jwk)
            encrypter.message = message
            encrypter.encrypt()
            jwe_cipher = encrypter.serialise()

            decrypter = ChaCha20Poly1305Jwe(jwk=self.symmetric_jwk)
            decrypter.load_compact(jwe_cipher)
            recovered = decrypter.decrypt()
            self.assertDictEqual(recovered, message)

    def test_decrypt_fails(self):
        """Failed integrity while decrypting a JWE message."""
        self.my_jwe.load_compact(JWE_CHACHA20POLY1305)
        self.my_jwe.jwk = self.symmetric_jwk
        altered_tag = JWE_CHACHA20POLY1305_DECODED['tag'][:-1] + b'\x42'
        self.my_jwe.tag = altered_tag
        self.assertRaises(nacl.exceptions.CryptoError, self.my_jwe.decrypt)

    @mock.patch('nacl.utils.random', autospec=True,
                return_value=test_vectors.JWE_CHACHA20POLY1305_DICT['nonce'])
    def test_vector_encrypt(self, random_mock):
        """JWE test vector encryption."""
        my_jwk = ChaCha20Poly1305Jwk(from_json=test_vectors.JWK_OCT256)
        my_jwe = ChaCha20Poly1305Jwe()
        my_jwe.jwk = my_jwk
        my_jwe.message = test_vectors.JWE_CHACHA20POLY1305_DICT['message']
        ciphertext, tag = my_jwe.encrypt()
        self.assertEqual(random_mock.call_count, 1)
        self.assertEqual(
            ciphertext,
            test_vectors.JWE_CHACHA20POLY1305_DICT['ciphertext'])
        self.assertEqual(
            tag, test_vectors.JWE_CHACHA20POLY1305_DICT['tag'])

    def test_vector_decrypt(self):
        """JWE test vector decryption."""
        my_jwk = ChaCha20Poly1305Jwk(from_json=test_vectors.JWK_OCT256)
        my_jwe = ChaCha20Poly1305Jwe(
            from_compact=test_vectors.JWE_CHACHA20POLY1305)
        my_jwe.jwk = my_jwk
        # The test vector uses 'ChaCha20/Poly1305' as an algorithm indicator.
        # Now it's standardised to use 'C20P', so it will fail.
        self.assertRaises(nacl.exceptions.CryptoError, my_jwe.decrypt)


class AES256GCMJweTest(unittest.TestCase):
    """Testing the AES256GCMJwe class."""

    symmetric_jwk = None
    my_jwe = None

    def setUp(self):  # noqa: D102
        self.symmetric_jwk = AES256GCMJwk(from_json=JWK_AES256GCM)
        self.my_jwe = AES256GCMJwe()

    def tearDown(self):  # noqa: D102
        pass

    def test_vanilla_constructor(self):
        """Make a vanilla/empty JWE."""
        jwe = AES256GCMJwe()
        self.assertEqual(jwe.header, None)
        self.assertEqual(jwe.key, None)
        self.assertEqual(jwe.nonce, None)
        self.assertEqual(jwe.ciphertext, None)
        self.assertEqual(jwe.tag, None)
        self.assertEqual(jwe.message, None)
        self.assertEqual(jwe.jwk, None)
        self.assertEqual(jwe._message_bytes, None)

    def test_constructor_via_factory(self):
        """Make an empty JWE via the factory."""
        jwe = Jwe.get_instance(enc='A256GCM')
        self.assertIsInstance(jwe, AES256GCMJwe)
        self.assertEqual(jwe._DEFAULT_HEADER['enc'], 'A256GCM')
        self.assertEqual(jwe.header, None)
        self.assertEqual(jwe.key, None)
        self.assertEqual(jwe.nonce, None)
        self.assertEqual(jwe.ciphertext, None)
        self.assertEqual(jwe.tag, None)
        self.assertEqual(jwe.message, None)
        self.assertEqual(jwe.jwk, None)
        self.assertEqual(jwe._message_bytes, None)

    @mock.patch('nacl.utils.random', autospec=True,
                return_value=JWE_AES256GCM_DECODED['nonce'])
    def test_encrypt(self, random_mock):
        """Encrypt a JWE message."""
        self.my_jwe.jwk = self.symmetric_jwk
        self.my_jwe.message = JWE_AES256GCM_MESSAGE
        self.assertIs(self.my_jwe.nonce, None)
        result = self.my_jwe.encrypt()
        self.assertEqual(random_mock.call_count, 1)
        ciphertext = JWE_AES256GCM_DECODED['ciphertext']
        tag = JWE_AES256GCM_DECODED['tag']
        self.assertIsNot(self.my_jwe.nonce, None)
        self.assertEqual(result[0], ciphertext)
        self.assertEqual(result[1], tag)
        self.assertEqual(self.my_jwe.ciphertext, ciphertext)
        self.assertEqual(self.my_jwe._message_bytes,
                         b'{"advice":"Don\'t panic!"}')

    def test_decrypt(self):
        """Decrypt a JWE message."""
        self.my_jwe.load_compact(JWE_AES256GCM)
        self.my_jwe.jwk = self.symmetric_jwk
        result = self.my_jwe.decrypt()
        self.assertDictEqual(result, JWE_AES256GCM_MESSAGE)
        self.assertEqual(self.my_jwe.message, JWE_AES256GCM_MESSAGE)
        self.assertEqual(self.my_jwe._message_bytes,
                         b'{"advice":"Don\'t panic!"}')

    def test_round_trips(self):
        """JWE en-/decrypt round trips."""
        for text in TEXT_CONTENT:
            message = {'text': text}
            encrypter = AES256GCMJwe(jwk=self.symmetric_jwk)
            encrypter.message = message
            encrypter.encrypt()
            jwe_cipher = encrypter.serialise()

            decrypter = AES256GCMJwe(jwk=self.symmetric_jwk)
            decrypter.load_compact(jwe_cipher)
            recovered = decrypter.decrypt()
            self.assertDictEqual(recovered, message)

    def test_decrypt_fails(self):
        """Failed integrity while decrypting a JWE message."""
        self.my_jwe.load_compact(JWE_AES256GCM)
        self.my_jwe.jwk = self.symmetric_jwk
        altered_tag = JWE_AES256GCM_DECODED['tag'][:-1] + b'\x42'
        self.my_jwe.tag = altered_tag
        self.assertRaises(cryptography.exceptions.InvalidTag,
                          self.my_jwe.decrypt)

    @mock.patch('nacl.utils.random', autospec=True,
                return_value=test_vectors.JWE_AES256GCM_DICT['nonce'])
    def test_vector_encrypt(self, random_mock):
        """JWE test vector encryption."""
        my_jwk = AES256GCMJwk(from_json=test_vectors.JWK_OCT256)
        my_jwe = AES256GCMJwe()
        my_jwe.jwk = my_jwk
        my_jwe.message = test_vectors.JWE_AES256GCM_DICT['message']
        ciphertext, tag = my_jwe.encrypt()
        self.assertEqual(random_mock.call_count, 1)
        self.assertEqual(
            ciphertext,
            test_vectors.JWE_AES256GCM_DICT['ciphertext'])
        self.assertEqual(
            tag, test_vectors.JWE_AES256GCM_DICT['tag'])

    def test_vector_decrypt(self):
        """JWE test vector decryption."""
        my_jwk = AES256GCMJwk(from_json=test_vectors.JWK_OCT256)
        my_jwe = AES256GCMJwe(
            from_compact=test_vectors.JWE_AES256GCM)
        my_jwe.jwk = my_jwk
        result = my_jwe.decrypt()
        self.assertDictEqual(result, {})


class UnsecuredJweTest(unittest.TestCase):
    """Testing the UnsecuredJwe class."""

    my_jwe = None

    def setUp(self):  # noqa: D102
        self.my_jwe = UnsecuredJwe()

    def tearDown(self):  # noqa: D102
        pass

    def test_vanilla_constructor(self):
        """Make a vanilla/empty JWE."""
        jwe = UnsecuredJwe()
        self.assertEqual(jwe.header, None)
        self.assertEqual(jwe.key, None)
        self.assertEqual(jwe.nonce, None)
        self.assertEqual(jwe.ciphertext, None)
        self.assertEqual(jwe.tag, None)
        self.assertEqual(jwe.message, None)
        self.assertEqual(jwe.jwk, None)
        self.assertEqual(jwe._message_bytes, None)

    def test_constructor_via_factory(self):
        """Make an empty JWE via the factory."""
        jwe = Jwe.get_instance(alg='unsecured')
        self.assertIsInstance(jwe, UnsecuredJwe)
        self.assertEqual(jwe._DEFAULT_HEADER['alg'], 'unsecured')
        self.assertNotIn('enc', jwe._DEFAULT_HEADER)
        self.assertEqual(jwe.header, None)
        self.assertEqual(jwe.key, None)
        self.assertEqual(jwe.nonce, None)
        self.assertEqual(jwe.ciphertext, None)
        self.assertEqual(jwe.tag, None)
        self.assertEqual(jwe.message, None)
        self.assertEqual(jwe.jwk, None)
        self.assertEqual(jwe._message_bytes, None)

    def test_encrypt(self):
        """(Pseudo-) encrypt a JWE message."""
        self.my_jwe.message = JWE_UNSECURED_MESSAGE
        result = self.my_jwe.encrypt()
        self.assertIs(self.my_jwe.nonce, None)
        self.assertEqual(result[0], b'{"advice":"Don\'t panic!"}')
        self.assertEqual(result[1], None)
        self.assertEqual(self.my_jwe.ciphertext,
                         JWE_UNSECURED_DECODED['ciphertext'])
        self.assertEqual(self.my_jwe._message_bytes,
                         JWE_UNSECURED_DECODED['ciphertext'])

    def test_decrypt(self):
        """(Pseudo-) decrypt a JWE message."""
        self.my_jwe.load_compact(JWE_UNSECURED)
        result = self.my_jwe.decrypt(allow_unsecured=True)
        self.assertDictEqual(result, JWE_UNSECURED_MESSAGE)
        self.assertEqual(self.my_jwe.message, JWE_UNSECURED_MESSAGE)
        self.assertEqual(self.my_jwe._message_bytes,
                         b'{"advice":"Don\'t panic!"}')

    def test_decrypt_forbidden(self):
        """Disallow (pseudo-) decrypt a JWE message."""
        self.my_jwe.load_compact(JWE_UNSECURED)
        self.assertRaises(RuntimeError, self.my_jwe.decrypt)

    def test_round_trips(self):
        """JWE en-/decrypt round trips."""
        for text in TEXT_CONTENT:
            message = {'text': text}
            encrypter = UnsecuredJwe()
            encrypter.message = message
            encrypter.encrypt()
            jwe_cipher = encrypter.serialise()

            decrypter = UnsecuredJwe()
            decrypter.load_compact(jwe_cipher)
            recovered = decrypter.decrypt(allow_unsecured=True)
            self.assertDictEqual(recovered, message)

    def test_decrypt_fails(self):
        """Failed integrity while decrypting a JWE message."""
        self.my_jwe.load_compact(JWE_UNSECURED)
        self.my_jwe.tag = b'\x42'
        self.assertRaises(RuntimeError, self.my_jwe.decrypt,
                          allow_unsecured=True)


class X25519ChaCha20Poly1305JweTest(unittest.TestCase):
    """Testing the X25519ChaCha20Poly1305Jwe class."""

    symmetric_jwk = None
    my_jwe = None

    def setUp(self):  # noqa: D102
        self.arthur_jwk = X25519Jwk(
            from_json=JWK_ARTHUR_FULL_X25519)
        self.ford_jwk = X25519Jwk(
            from_json=JWK_FORD_PUB_X25519)
        self.my_jwe = X25519ChaCha20Poly1305Jwe()

    def tearDown(self):  # noqa: D102
        pass

    def test_constructor_via_factory(self):
        """Make an empty JWE via the factory."""
        jwe = Jwe.get_instance(enc='C20P', alg='ECDH-ES')
        self.assertIsInstance(jwe, X25519ChaCha20Poly1305Jwe)
        self.assertEqual(jwe._DEFAULT_HEADER['alg'], 'ECDH-ES')
        self.assertEqual(jwe._DEFAULT_HEADER['enc'], 'C20P')
        self.assertEqual(jwe.header, None)
        self.assertEqual(jwe.key, None)
        self.assertEqual(jwe.nonce, None)
        self.assertEqual(jwe.ciphertext, None)
        self.assertEqual(jwe.tag, None)
        self.assertEqual(jwe.message, None)
        self.assertEqual(jwe.jwk, None)
        self.assertEqual(jwe._message_bytes, None)

    @mock.patch('sspyjose.jwe.X25519ChaCha20Poly1305Jwe._KEYBYTES', 16)
    @mock.patch('sspyjose.jwe.X25519ChaCha20Poly1305Jwe._DEFAULT_HEADER',
                {'enc': 'A128GCM'})
    @mock.patch('nacl.public.Box.shared_key', autospec=True)
    def test_ecdh_es_key_rfc7518_appendix_c(self, shared_key_mock, *_):
        """Test vector from RFC 7518 Appendix C."""
        shared_key_mock.return_value = bytes(
            [158, 86, 217, 29, 129, 113, 53, 211, 114, 131, 66, 131, 191, 132,
             38, 156, 251, 49, 110, 163, 218, 128, 106, 72, 246, 218, 167, 121,
             140, 254, 144, 196])
        ephemeral_key = X25519Jwk(generate=True)
        result = self.my_jwe.ecdh_es_key(ephemeral_key, self.arthur_jwk,
                                         apu=b'Alice', apv=b'Bob')
        self.assertEqual(shared_key_mock.call_count, 1)
        self.assertEqual(result,
                         bytes([86, 170, 141, 234, 248, 35, 109, 32,
                                92, 34, 40, 205, 113, 167, 16, 26]))

    def test_ecdh_es_key(self):
        """Vanilla ECDH-ES key."""
        result = self.my_jwe.ecdh_es_key(self.arthur_jwk, self.ford_jwk)
        self.assertEqual(result, base64.urlsafe_b64decode(
            'xjynmBeFDBma3o21OEjDe5sXnwsT1Vo8D8_RKiKfJA4='))

    def test_ecdh_es_key_with_sender_receiver_info(self):
        """ECDH-ES key with sender/receiver info."""
        result = self.my_jwe.ecdh_es_key(self.arthur_jwk, self.ford_jwk,
                                         apu=b'Arthur', apv=b'Ford')
        self.assertEqual(result, base64.urlsafe_b64decode(
            'WcqyVAoZ74rDfhaR1np-TXkvE41dszdqdAQ_KAuTYKQ='))

    @mock.patch('sspyjose.jwe.X25519Jwk', autospec=True)
    @mock.patch('nacl.utils.random', autospec=True,
                return_value=JWE_CHACHA20POLY1305_DECODED['nonce'])
    def test_encrypt(self, random_mock, ephemeral_key_mock):
        """X25519 encrypt a JWE message."""
        ephemeral_key_mock.return_value = self.arthur_jwk
        self.my_jwe.jwk = self.ford_jwk
        self.my_jwe.message = JWE_X25519CHACHA20POLY1305_MESSAGE
        self.assertIs(self.my_jwe.nonce, None)
        ciphertext, tag = self.my_jwe.encrypt()
        self.assertEqual(random_mock.call_count, 1)
        self.assertEqual(ephemeral_key_mock.call_count, 1)
        ciphertext_check = JWE_X25519CHACHA20POLY1305_DECODED['ciphertext']
        tag_check = JWE_X25519CHACHA20POLY1305_DECODED['tag']
        self.assertIsNot(self.my_jwe.nonce, None)
        self.assertEqual(ciphertext, ciphertext_check)
        self.assertEqual(tag, tag_check)
        self.assertEqual(self.my_jwe.ciphertext, ciphertext_check)
        self.assertEqual(self.my_jwe._message_bytes,
                         b'{"advice":"Don\'t panic!"}')

    @mock.patch('sspyjose.jwe.X25519Jwk', autospec=True)
    @mock.patch('nacl.utils.random', autospec=True,
                return_value=JWE_CHACHA20POLY1305_DECODED['nonce'])
    def test_serialise(self, random_mock, ephemeral_key_mock):
        """Serialise an X25519 encrypted JWE message."""
        ephemeral_key_mock.return_value = self.arthur_jwk
        self.my_jwe.jwk = self.ford_jwk
        self.my_jwe.message = JWE_X25519CHACHA20POLY1305_MESSAGE
        self.my_jwe.encrypt()
        self.assertEqual(random_mock.call_count, 1)
        self.assertEqual(ephemeral_key_mock.call_count, 1)
        result = self.my_jwe.serialise()
        self.assertEqual(result, JWE_X25519CHACHA20POLY1305)

    def test_serialise_with_sender_receiver_info(self):
        """Serialise X25519 encrypted JWE message with sender/receiver info."""
        self.my_jwe.jwk = self.ford_jwk
        self.my_jwe.message = JWE_X25519CHACHA20POLY1305_MESSAGE
        self.my_jwe.encrypt(apu=b'Arthur', apv=b'Ford')
        result = self.my_jwe.serialise()
        header = json.loads(
            base64.urlsafe_b64decode(result.split('.')[0] + '=='))
        del header['epk']['x']
        check = {'alg': 'ECDH-ES',
                 'enc': 'C20P',
                 'epk': {'kty': 'OKP', 'crv': 'X25519'},
                 'apu': 'QXJ0aHVy',
                 'apv': 'Rm9yZA'}
        self.assertDictEqual(header, check)

    def test_decrypt(self):
        """X25519 decrypt a JWE message."""
        self.my_jwe.load_compact(JWE_X25519CHACHA20POLY1305)
        ford_priv_jwk = X25519Jwk(from_json=JWK_FORD_FULL_X25519)
        self.my_jwe.jwk = ford_priv_jwk
        result = self.my_jwe.decrypt()
        self.assertDictEqual(result, JWE_X25519CHACHA20POLY1305_MESSAGE)
        self.assertEqual(self.my_jwe.message,
                         JWE_X25519CHACHA20POLY1305_MESSAGE)
        self.assertEqual(self.my_jwe._message_bytes,
                         b'{"advice":"Don\'t panic!"}')

    def test_round_trips(self):
        """X25519 JWE en-/decrypt round trips."""
        ford_priv_jwk = X25519Jwk(from_json=JWK_FORD_FULL_X25519)
        for text in TEXT_CONTENT:
            message = {'text': text}
            encrypter = X25519ChaCha20Poly1305Jwe(jwk=ford_priv_jwk)
            encrypter.message = message
            encrypter.encrypt()
            jwe_cipher = encrypter.serialise()

            decrypter = X25519ChaCha20Poly1305Jwe(jwk=ford_priv_jwk)
            decrypter.load_compact(jwe_cipher)
            recovered = decrypter.decrypt()
            self.assertDictEqual(recovered, message)


class FactoryUsageTest(unittest.TestCase):
    """Testing the JWE via the factories exclusively."""

    def setUp(self):  # noqa: D102
        pass

    def tearDown(self):  # noqa: D102
        pass

    def test_encrypt_decrypt_defaults(self):
        """En-/decrypting cycles using defaults."""
        symmetric_jwk = Jwk.get_instance(alg=Jwe.DEFAULT_ENC,
                                         from_json=test_vectors.JWK_OCT256)
        for text in TEXT_CONTENT:
            message = {'text': text}
            encrypter = Jwe.get_instance(jwk=symmetric_jwk)
            encrypter.message = message
            encrypter.encrypt()
            jwe_cipher = encrypter.serialise()

            decrypter = Jwe.get_instance(jwk=symmetric_jwk)
            decrypter.load_compact(jwe_cipher)
            recovered = decrypter.decrypt()
            self.assertDictEqual(recovered, message)

    def test_encrypt_decrypt_c20p(self):
        """En-/decrypting cycles using ChaCha20/Poly1305."""
        symmetric_jwk = Jwk.get_instance(alg='C20P',
                                         from_json=test_vectors.JWK_OCT256)
        for text in TEXT_CONTENT:
            message = {'text': text}
            encrypter = Jwe.get_instance(enc='C20P', jwk=symmetric_jwk)
            encrypter.message = message
            encrypter.encrypt()
            jwe_cipher = encrypter.serialise()

            decrypter = Jwe.get_instance(enc='C20P', jwk=symmetric_jwk)
            decrypter.load_compact(jwe_cipher)
            recovered = decrypter.decrypt()
            self.assertDictEqual(recovered, message)

    def test_encrypt_decrypt_a256gcm(self):
        """En-/decrypting cycles using AES256-GCM."""
        symmetric_jwk = Jwk.get_instance(alg='A256GCM',
                                         from_json=test_vectors.JWK_OCT256)
        for text in TEXT_CONTENT:
            message = {'text': text}
            encrypter = Jwe.get_instance(enc='A256GCM', jwk=symmetric_jwk)
            encrypter.message = message
            encrypter.encrypt()
            jwe_cipher = encrypter.serialise()

            decrypter = Jwe.get_instance(enc='A256GCM', jwk=symmetric_jwk)
            decrypter.load_compact(jwe_cipher)
            recovered = decrypter.decrypt()
            self.assertDictEqual(recovered, message)

    def test_encrypt_decrypt_x25519_c20p(self):
        """X25519 JWE en-/decrypt round trips using ChaCha20/Poly1305."""
        priv_jwk = Jwk.get_instance(
            crv='X25519', from_dict=test_vectors.JWK_X25519_ALICE_SK)
        for text in TEXT_CONTENT:
            message = {'text': text}
            encrypter = Jwe.get_instance(alg='ECDH-ES', enc='C20P',
                                         jwk=priv_jwk)
            encrypter.message = message
            encrypter.encrypt()
            jwe_cipher = encrypter.serialise()

            decrypter = Jwe.get_instance(alg='ECDH-ES', enc='C20P',
                                         jwk=priv_jwk)
            decrypter.load_compact(jwe_cipher)
            recovered = decrypter.decrypt()
            self.assertDictEqual(recovered, message)

    def test_encrypt_decrypt_x25519_a256gcm(self):
        """X25519 JWE en-/decrypt round trips using AES256-GCM."""
        priv_jwk = Jwk.get_instance(
            crv='X25519', from_dict=test_vectors.JWK_X25519_ALICE_SK)
        for text in TEXT_CONTENT:
            message = {'text': text}
            encrypter = Jwe.get_instance(alg='ECDH-ES', enc='A256GCM',
                                         jwk=priv_jwk)
            encrypter.message = message
            encrypter.encrypt()
            jwe_cipher = encrypter.serialise()

            decrypter = Jwe.get_instance(alg='ECDH-ES', enc='A256GCM',
                                         jwk=priv_jwk)
            decrypter.load_compact(jwe_cipher)
            recovered = decrypter.decrypt()
            self.assertDictEqual(recovered, message)


if __name__ == '__main__':
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
