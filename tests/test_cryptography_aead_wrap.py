# -*- coding: utf-8 -*-
"""Tests for the Cryptography wrapper module."""

# Created: 2019-03-08 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

from unittest import mock  # @UnusedImport # noqa: F401
import unittest

from sspyjose.cryptography_aead_wrap import (aes256gcm_encrypt,
                                             aes256gcm_decrypt)
from tests.rfc7516_test_data import TEST_VECTOR


class SodiumWrapTest(unittest.TestCase):
    """Testing the Cryptography wrapper module."""

    def setUp(self):  # noqa: D102
        pass

    def tearDown(self):  # noqa: D102
        pass

    def test_ietf_jwe_test_encrypt_vector(self):
        """Encrypt using the vectors."""
        result = aes256gcm_encrypt(
            TEST_VECTOR['message'], TEST_VECTOR['aad'],
            TEST_VECTOR['nonce'], TEST_VECTOR['key'])
        # FIXME: tag mismatch, ciphertext OK
        self.assertTupleEqual(
            result, (TEST_VECTOR['ciphertext'], TEST_VECTOR['tag']))

    def test_ietf_jwe_test_decrypt_vector(self):
        """Decrypt using the vectors."""
        result = aes256gcm_decrypt(
            TEST_VECTOR['ciphertext'], TEST_VECTOR['tag'],
            TEST_VECTOR['aad'], TEST_VECTOR['nonce'], TEST_VECTOR['key'])
        self.assertEqual(result, TEST_VECTOR['message'])


if __name__ == '__main__':
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
